package com.odroid.samlspring.controllers;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.saml2.provider.service.authentication.Saml2AuthenticatedPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/private")
@Slf4j
public class PrivateController {

    @GetMapping("/")
    public String index(@AuthenticationPrincipal Saml2AuthenticatedPrincipal principal, Model model) {
        String readablePrincipal = saml2AuthenticatedPrincipalToString(principal);
        log.debug(readablePrincipal);
        model.addAttribute("user", readablePrincipal);
        return "private";
    }

    private String saml2AuthenticatedPrincipalToString(@NonNull Saml2AuthenticatedPrincipal principal) {
        StringBuffer res = new StringBuffer();
        res.append("Saml2AuthenticatedPrincipal {\n");
        res.append("  name: " + principal.getName() + "\n");
        res.append("  sessionIndexes: " + principal.getSessionIndexes() + "\n");
        res.append("  relyingPartyRegistrationId: " + principal.getRelyingPartyRegistrationId() + "\n");
        res.append("  attributes: {\n");
        principal.getAttributes().forEach((attribute, values) -> {
            res.append("    " + attribute + ": " + values + "\n");
        });
        res.append("  }\n");
        res.append("}");
        return res.toString();
    }
}
