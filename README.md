Simple SAML client using Keycloak as IdP (or others, theoretically). It will use "POST binding", which is more secure than the usual "redirect binding".

# Requirements

* If you want to explore or modify the source code:
  * IntelliJ Idea (2023.3.2 Community Edition tested)
  * See "Development", below
* If you only want to run the application: 
  * Docker (Rancher Desktop 1.11.1 is tested)
  * See "Running", below

# Development

* Clone the repo
* Open the repo in IntelliJ
* Run `./gradlew bootRun` (you can use the `gradle` panel on the right side)
* Browse http://localhost:8080/
* Click on `private`
* Authenticate with a/a
* Enjoy

By default, it uses my own cloud-hosted SAML IdP (provided by CloudIAM, based in KeyCloak), but you can use your own SAML IdP if it provides metadata autodiscovery. Just modify the file `application.properties`, key `idp.metadataLocation`.

# Running it yourself

* Run `docker run --rm -ti -p 8080:8080 -e IDP_METADATALOCATION=https://lemur-12.cloud-iam.com/auth/realms/j/protocol/saml/descriptor registry.gitlab.com/javier-sedano/samlspring/app`
  * `8080:8080` is the port mapping. If you want, change it to YOURPORT:8080 to user your port.
  * `https://lemur-12.cloud-iam.com/auth/realms/j/protocol/saml/descriptor` is the location of my cloud-hosted SAML IdP (provided by CloudIAM, based in KeyCloak), but you can use your own SAML IdP if it provides metadata autodiscovery: just modify this environment variable.
  * You can add additional Java options with `-e JAVA_OPS="-Xmx30m"` (or whatever you want to add).
* Browse http://localhost:8080/
* Click on `private`
* Authenticate with `a/a`
* Enjoy

# Changing the IdP

You can use your own IdP for testing or experimentation. Just change the URL in the `application.properties` or the docker command line.

For example, to use a quick&dirty KeyCloak locally in Docker:
* Run `docker run --rm -ti -p 8000:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak start-dev`
  * Browse http://localhost:8000 , go to the Admin Console, and authenticate with `admin/admin`
  * In the dropdown, choose "Create realm" and create a new Realm called `J` (or whatever you prefer).
  * In the section "Users", create a new user, for example. Provide the email, first name, last name,... Once created, in the Credentials tab, set a password (remember to remove the flag "Temporary" or you will need to change the password in the first logon).
  * In the section "Clients", create a new client, with type SAML:
    * Client ID: the URL of the `samlspring` application, in this case `http://localhost:8080/saml2/service-provider-metadata/cloudiam`.
    * Root URL: http://localhost:8080
    * Home URL: /
    * Valid redirect URIs: /*
    * Valid post logout redirect URIs: -
    * Configure the authentication of the client `samlspring`:
      * Download the certificate used by `samlspring` from [rp-certificate.crt](./src/main/resources/rp-certificate.crt) and [rp-certificate.jks](./src/main/resources/rp-certificate.jks).
        * **BIG SECURITY WARNING**: read [Security warning](#security-warning)
      * In KeyCloak, go to the `Keys` tab:
        * Verify that "Client signature required" is enabled. Use "Import Key" and import the certificate (see below).
        * Enable "Encryption keys config" and import the key (see below).
        * WARNING: In the moment of writing this guide, the version of KeyCloak I am using seems to have a bug: sometimes you can select a PEM certificate (so you can select the `rp-certificate.crt` file) or sometimes you can only choose a JKS (so you will need to use the `rp-certificate.jks` file)... so you will need to use your own intelligence on top of these instructions.
          * Two options:
            * Looks like you can enable it and choose a JKS format.
            * ... or you can enable, generate a random key/certificate to throw away, and the import the file, and then maybe you can choose a PEM format.
          * Either way, both the PEM (.crt) and JKS (.jks) formats are provided.
  * In the section "Realm Settings" there is a link to "SAML 2.0 Identity Provider Metadata"... this is the URL you need to use a metadataLocation in the next step.
* Modify the `application.properties`to use the URL of the metadata from the previous step.
* Browse http://localhost:8080 etc etc

**Warning**: this same procedure does not work if using both the dockerized samlspring from [above](#running-it-yourself) and a [dockerized KeyCloak](#changing-the-idp), because probably all of your URLs point to `localhost` and localhost inside a container may not mean what you think it means (if you understood why, then you didn't need this warning).

# Security warning

This example uses a key/certificate pair that is kind-of hard-coded into the repository (it can be replaced using standard Spring Boot externalized configuration; see `application.properties` and https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config). Do not do this in any production service. It is done here only to ease running the demonstration. Use this code as a base if you want, but change the key/certificate.
