package com.odroid.samlspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SamlspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamlspringApplication.class, args);
	}

}
