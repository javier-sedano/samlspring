package com.odroid.samlspring;

import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.security.x509.X509Support;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.converter.RsaKeyConverters;
import org.springframework.security.saml2.core.Saml2X509Credential;
import org.springframework.security.saml2.provider.service.registration.InMemoryRelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistration;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrations;
import org.springframework.security.saml2.provider.service.web.DefaultRelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.RelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.authentication.OpenSaml4AuthenticationRequestResolver;
import org.springframework.security.saml2.provider.service.web.authentication.Saml2AuthenticationRequestResolver;
import org.springframework.security.web.SecurityFilterChain;

import java.io.InputStream;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

    public static final String ROOT_URL = "/";
    public static final String PRIVATE_URL_PATTERN = ROOT_URL + "private/**";

    @Value("${idp.metadataLocation}")
    public String idpMetadataLocation;

    @Value("${rp.certLocation}")
    private String rpCertLocation;
    @Value("${rp.keyLocation}")
    private String rpKeyLocation;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorizationManagerRequestMatcherRegistry -> {
                    authorizationManagerRequestMatcherRegistry
                            .requestMatchers(ROOT_URL).permitAll()
                            .requestMatchers(PRIVATE_URL_PATTERN).authenticated()
                            .anyRequest().authenticated();
                })
                .saml2Login(withDefaults())
                .logout(httpSecurityLogoutConfigurer -> {
                    httpSecurityLogoutConfigurer.logoutSuccessUrl("/");
                });
        return http.build();
    }

    @Bean
    public RelyingPartyRegistrationRepository relyingPartyRegistrations() throws Exception {
        Resource signingCertResource = new ClassPathResource(this.rpCertLocation);
        Resource signingKeyResource = new ClassPathResource(this.rpKeyLocation);
        try (
                InputStream is = signingKeyResource.getInputStream();
                InputStream certIS = signingCertResource.getInputStream();
        ) {
            X509Certificate rpCertificate = X509Support.decodeCertificate(certIS.readAllBytes());
            RSAPrivateKey rpKey = RsaKeyConverters.pkcs8().convert(is);
            final Saml2X509Credential rpSigningCredentials = Saml2X509Credential.signing(rpKey, rpCertificate);
            final Saml2X509Credential rpDecryptionCredentials = Saml2X509Credential.decryption(rpKey, rpCertificate);

            RelyingPartyRegistration registration = RelyingPartyRegistrations
//                .fromMetadataLocation("https://lemur-12.cloud-iam.com/auth/realms/j/protocol/saml/descriptor")
//                .fromMetadataLocation("http://localhost:8000/realms/J/protocol/saml/descriptor")
                    .fromMetadataLocation(idpMetadataLocation)
//                .assertingPartyDetails(builder -> {
//                    // Keycloak sets WantAuthnRequestsSigned="true" even when configured as non-required (does not have a setting to disable this)
//                    builder.wantAuthnRequestsSigned(false);
//                })
                    .decryptionX509Credentials(saml2X509Credentials -> saml2X509Credentials.add(rpDecryptionCredentials))
                    .registrationId("cloudiam")
                    .signingX509Credentials(saml2X509Credentials -> saml2X509Credentials.add(rpSigningCredentials))
                    .authnRequestsSigned(false)
                    .build();
            return new InMemoryRelyingPartyRegistrationRepository(registration);
        }
    }

    @Bean
    Saml2AuthenticationRequestResolver authenticationRequestResolver(RelyingPartyRegistrationRepository registrations) {
        RelyingPartyRegistrationResolver registrationResolver =
                new DefaultRelyingPartyRegistrationResolver(registrations);
        OpenSaml4AuthenticationRequestResolver authenticationRequestResolver =
                new OpenSaml4AuthenticationRequestResolver(registrationResolver);
        authenticationRequestResolver.setAuthnRequestCustomizer((context) -> {
            AuthnRequest authnRequest = context.getAuthnRequest();
            authnRequest.setForceAuthn(true);
        });
        return authenticationRequestResolver;
    }
}